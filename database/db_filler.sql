insert into countries (country_id, name)
values (1,'England'),
       (2,'France'),
       (3,'Denmark'),
       (4,'Spain'),
       (5,'Germany');

insert into cities (city_id, name, country_id)
values (1,'London',1),
       (2,'Paris',2),
       (3,'Lion',2),
       (4,'Manchester',1),
       (5,'Berlin',5),
       (6,'Madrid',4),
       (7,'Copenhagen',3);


insert into delivery_options (deliver_options_id, name)
values (1,'pick up'),
       (2,'by post');


insert into warehouses (warehouse_id, city_id, street_name)
values (1,3,'25 rue de la République'),
       (2,2,'27 rue de la Mosque'),
       (3,4,'52 Church street'),
       (4,1,'78 Victoria Way'),
       (5,6,'255 Caille del rei'),
       (6,5,'33 Brandenburgische Straße'),
       (7,7,'87 Helgeshøj Allé');


insert into roles (role_id, name)
values (1,'employee'),
       (2,'customer');

insert into statuses (status_id, name)
values (1,'preparing'),
       (2,'on the way'),
       (3,'completed');

insert into shipments (shipment_id, departure_date, arrival_date, shipment_status, origin_warehouse_id, destination_warehouse_id)
values (1,'2021-02-02 00:00:00','2021-02-17 00:00:00',1, 5,4),
       (2,'2021-01-09 00:00:00','2021-02-01 00:00:00',2, 5, 7),
       (3,'2021-02-10 00:00:00','2021-03-01 00:00:00',2, 1,2),
       (4,'2021-03-01 00:00:00','2021-03-19 00:00:00',1,2,3),
       (5,'2021-04-23 00:00:00','2021-04-30 00:00:00',2,3,6),
       (6,'2021-01-05 00:00:00','2021-01-21 00:00:00',3,4,2),
       (7,'2021-01-02 00:00:00','2021-01-17 00:00:00',3,4,1);

insert into categories (category_id, name)
values (1,'Electronics'),
       (2,'Clothing'),
       (3,'Medical'),
       (4,'Literature');

insert into persons (person_id, first_name, last_name, email, city_id, street_name, person_role)
values (1,'Joseph','Charles','joseph.charles@gmail.com',3,'48 rue La Boétie',2),
       (2,'Daniel','Anthony','Daniel.Anthony@gmail.com',2,'73 Place de la Madeleine',2),
       (3,'Christopher','Charles','Christopher.charles@gmail.com',1,'246 Kings Road',2),
       (4,'Joseph','Donald','joseph.Donald77@gmail.com',7,'Pilekrogen 76',2),
       (5,'Joshua','Kenneth','Joshua.theKenneth@gmail.com',2,'95 Faubourg Saint Honoré',2),
       (6,'Kevin','Edward','Kevin.Edward@gmail.com',6,'Ventanilla de Beas 50',2),
       (7,'Benjamin','Scott','Benjamin.Scott56@gmail.com',4,'109 Grove Road',2),
       (8,'Samuel','Gregory','Gregory900@yahoo.com',4,'61 Mill Road',2),
       (9,'Patrick','Raymond','onePatrickRaymond9@gmail.com',6,'Plaza del Rey 1',2),
       (10,'Dennis','Tyler','Tyler.Dennis@yahoo.com',5,'Mohrenstrasse 43',2),
       (11,'Roger','Noah','Roger.Noah78@yahoo.com',7,' Eskelundsvej 88',1),
       (12,'Gerald','Austin','Gerald777Austin@yahoo.com',2,'63 place de Miremont',1),
       (13,'Jacqueline','Ann','Jacqueline.Ann@yahoo.com',6,'Paseo de la Castellana 75',1),
       (14,'Sara','Madison','Sara.Madison@gmail.com',5,'Prager Str 25',1),
       (15,'Bruce','Willie','Bruce.Willie@yahoo.com',1,'588 Kingsway',1),
       (16,'Amber','Rose','Amber.Rose@yahoo.com',3,' 67 Rue du Palais',1),
       (17,'Johnny','Tyler','Johnnyty@yahoo.com',4,'11 New Road',1);

insert into parcels (parcel_id, customer_id, to_warehouse_id, category_id, weight, shipments_id, delivery_option)
values (1,7,3,2,12,2,1),
       (2,8,3,3,17,2,2),
       (3,3,4,2,54,2,1),
       (4,5,2,3,37,7,2),
       (5,2,2,4,5,7,1),
       (6,4,7,1,5,1,1),
       (7,1,1,1,60,3,2),
       (8,6,5,1,23,6,2),
       (9,9,5,4,4,5,1),
       (10,10,6,2,7,4,2);
