create table categories
(
    category_id int auto_increment
        primary key,
    name        varchar(20) not null,
    constraint categories_name_uindex
        unique (name)
);

create table countries
(
    country_id int auto_increment
        primary key,
    name       varchar(25) not null,
    constraint country_name_uindex
        unique (name)
);

create table cities
(
    city_id    int auto_increment
        primary key,
    name       varchar(25) not null,
    country_id int         null,
    constraint city_country_fk
        foreign key (country_id) references countries (country_id)
            on update cascade
);

create table delivery_options
(
    deliver_options_id int auto_increment
        primary key,
    name               varchar(20) not null
);


create table roles
(
    role_id int auto_increment
        primary key,
    name    varchar(20) not null,
    constraint roles_name_uindex
        unique (name)
);

create table persons
(
    person_id   int auto_increment
        primary key,
    first_name  varchar(20) not null,
    last_name   varchar(20) not null,
    email       varchar(40) null,
    city_id     int         null,
    street_name varchar(30) null,
    person_role int         null,
    constraint persion_email_uindex
        unique (email),
    constraint customer_city_fk
        foreign key (city_id) references cities (city_id)
            on update cascade,
    constraint persons_roles_fk
        foreign key (person_role) references roles (role_id)
            on update cascade
);

create table statuses
(
    status_id int auto_increment
        primary key,
    name      varchar(20) not null,
    constraint status_name_uindex
        unique (name)
);

create table warehouses
(
    warehouse_id int auto_increment
        primary key,
    city_id      int         null,
    street_name  varchar(30) not null,
    constraint warehouse_city_fk
        foreign key (city_id) references cities (city_id)
            on update cascade on delete cascade
);

create table shipments
(
    shipment_id              int auto_increment
        primary key,
    departure_date           date null,
    arrival_date             date null,
    shipment_status          int  null,
    origin_warehouse_id      int  null,
    destination_warehouse_id int  null,
    constraint shipments_destination_warehouse_fk
        foreign key (destination_warehouse_id) references warehouses (warehouse_id)
            on update cascade on delete cascade,
    constraint shipments_oigin_warehouse_fk
        foreign key (origin_warehouse_id) references warehouses (warehouse_id)
            on update cascade on delete cascade,
    constraint shipments_statuses_fk
        foreign key (shipment_status) references statuses (status_id)
            on update cascade
);


create table parcels
(
    parcel_id       int auto_increment
        primary key,
    customer_id     int    null,
    to_warehouse_id int    null,
    category_id     int    null,
    weight          double null,
    shipments_id    int    null,
    delivery_option int    null,
    constraint parcels_categories_fk
        foreign key (category_id) references categories (category_id)
            on update cascade,
    constraint parcels_delivery_options_fk
        foreign key (delivery_option) references delivery_options (deliver_options_id)
            on update cascade,
    constraint parcels_persons_fk
        foreign key (customer_id) references persons (person_id)
            on update cascade,
    constraint parcels_shipments_fk
        foreign key (shipments_id) references shipments (shipment_id)
            on update cascade on delete cascade,
    constraint parcels_warehouses_fk
        foreign key (to_warehouse_id) references warehouses (warehouse_id)
            on update cascade on delete cascade
);

