package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CreateParcelsDto {

    @NotNull
    @Positive(message = "Id should be positive")
    private int customerId;

    @NotNull
    @Positive(message = "Id should be positive")
    private int warehouseId;

    @NotNull
    @Positive(message = "Category Id should be positive")
    private int categoryId;

    @NotNull
    @Positive(message = "Weight should be positive")
    private int weight;

    @NotNull
    @Positive(message = "Shipment Id should be positive")
    private int shipmentId;

    @NotNull
    @Positive(message = "Delivery Option Id should be positive")
    private int deliveryOptionId;

    public CreateParcelsDto() {
    }

    public CreateParcelsDto(int customerId, int warehouseId, int categoryId,int weight,int shipmentId, int deliveryOptionId ) {
        this.customerId = customerId;
        this.warehouseId = warehouseId;
        this.categoryId = categoryId;
        this.weight = weight;
        this.shipmentId = shipmentId;
        this.deliveryOptionId = deliveryOptionId;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(int warehouseId) {
        this.warehouseId = warehouseId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getShipmentId() {
        return shipmentId;
    }

    public void setShipmentId(int warehouseId) {
        this.shipmentId = shipmentId;
    }

    public int getDeliveryOptionId() {
        return deliveryOptionId;
    }

    public void setDeliveryOptionId(int deliveryOptionId) {
        this.deliveryOptionId = deliveryOptionId;
    }
}
