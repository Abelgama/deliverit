package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ShipmentStatus;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentStatusRespository;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentStatusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
public class ShipmentStatusServiceImpl implements ShipmentStatusService {

    private final ShipmentStatusRespository shipmentStatusRespository;

    @Autowired
    public ShipmentStatusServiceImpl(ShipmentStatusRespository repository) {
        this.shipmentStatusRespository = repository;
    }

    @Override
    public List<ShipmentStatus> getAll() {
        return shipmentStatusRespository.getAll();
    }

    @Override
    public ShipmentStatus getById(int id) {
        try {
            return shipmentStatusRespository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
