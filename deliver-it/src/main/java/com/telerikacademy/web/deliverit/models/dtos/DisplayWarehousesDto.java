package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class DisplayWarehousesDto {

    @NotBlank(message = "City name can't be empty")
    @Size(min=3, max=20, message = "City name should be between 3 and 20 symbols")
    private String city;
    @NotBlank(message = "Country name can't be empty")
    @Size(min=3, max=20, message = "Country name should be between 3 and 20 symbols")
    private String country;
    @NotBlank(message = "Address can't be empty")
    @Size(min=3, max=20, message = "Address should be between 3 and 50 symbols")
    private String address;

    public DisplayWarehousesDto() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
