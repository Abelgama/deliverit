package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.Country;

import java.util.List;

public interface CountryRepository {
    List<Country> getAll();

    Country getById(int id);
}
