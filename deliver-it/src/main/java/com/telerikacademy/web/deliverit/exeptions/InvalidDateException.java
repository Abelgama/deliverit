package com.telerikacademy.web.deliverit.exeptions;

public class InvalidDateException extends  RuntimeException{
    public InvalidDateException(String message) {
        super(message);
    }
}
