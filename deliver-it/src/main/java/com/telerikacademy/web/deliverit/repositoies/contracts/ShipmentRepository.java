package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ShipmentRepository {
    List<Shipment> getAll();

    Shipment getById(int id);

    void delete(int id);

    List<Shipment> getMyShipmentStatus(String email);

    List<Shipment> getNextShipmentToArrive(int id);

    void update(Shipment shipment);

    void create(Shipment shipment);

    List<Shipment> filter (Optional<Integer> id);
}
