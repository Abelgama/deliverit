package com.telerikacademy.web.deliverit.controlers;

import com.telerikacademy.web.deliverit.controlers.helpers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.InvalidDateException;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.dtos.CreateShipmentDto;
import com.telerikacademy.web.deliverit.models.dtos.DisplayShipmentsDto;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.utils.ShipmentMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api/shipments")
public class ShipmentController {
    private final ShipmentService shipmentService;
    private final ShipmentMapper mapper;
    private final AuthenticationHelper authenticationHelper;


    public ShipmentController(ShipmentService shipmentService, ShipmentMapper mapper, AuthenticationHelper authenticationHelper) {
        this.shipmentService = shipmentService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<DisplayShipmentsDto> getAll() {
        return mapper.allShipmentsFromDto(shipmentService.getAll());
    }

    @GetMapping("/{id}")
    public DisplayShipmentsDto getById(@PathVariable int id) {
        return mapper.shipmentsFromDto(id);
    }

    // localhost:8080/v1/api/shipments/filter?id=2
    //filter shipments from origin warehouse
    @GetMapping("/filter")
    public List<DisplayShipmentsDto> filter(@RequestParam(required = false) Optional<Integer> id) {
        return mapper.allShipmentsFromDto(shipmentService.filter(id));
    }

    @GetMapping("/status")
    public List<Shipment> getMyShipmentStatus(@RequestHeader HttpHeaders header, @RequestParam String email) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            return shipmentService.getMyShipmentStatus(email, person);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/arrival")
    public List<Shipment> getNextShipmentToArrive(@RequestHeader HttpHeaders header, @RequestParam int id) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            return shipmentService.getNextShipmentToArrive(id, person);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        shipmentService.delete(id);
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders header, @PathVariable int id, @Valid @RequestBody CreateShipmentDto dto) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            Shipment shipmentToUpdate = mapper.shipmentFromDto(dto, id);
            shipmentService.update(shipmentToUpdate, person);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public boolean create(@RequestHeader HttpHeaders header, @Valid @RequestBody CreateShipmentDto dto) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            Shipment shipment = mapper.shipmentFromDto(dto);
            shipmentService.create(shipment, person);
            return true;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityExeption e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (InvalidDateException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
