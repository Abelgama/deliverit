package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.ParcelCategory;

import java.util.List;

public interface ParcelCategoryRepository {

    List<ParcelCategory> getAll();

    ParcelCategory getById(int id);
}
