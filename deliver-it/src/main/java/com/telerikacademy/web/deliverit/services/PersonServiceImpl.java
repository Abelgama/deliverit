package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelRepository;
import com.telerikacademy.web.deliverit.repositoies.contracts.PersonRepository;
import com.telerikacademy.web.deliverit.services.contracts.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonServiceImpl(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public List<Person> getAll() {
        return personRepository.getAll();
    }

    @Override
    public Person getById(int id) {return personRepository.getById(id);

    }

    @Override
    public List<Person> getByEmail(String email) {return personRepository.getByEmail(email);

    }

    @Override
    public List<Person> searchByCustomerName(Optional<String> firstName, Optional<String> lastName) {
        return personRepository.searchByCustomerName(firstName,lastName);
    }

    @Override
    public void delete(int id) {
        personRepository.delete(id);
    }

    @Override
    public void update(Person person) {
        boolean duplicateExists = true;
        try {
            Person existingPerson = personRepository.getById(person.getId());
            if (existingPerson.getId() == person.getId()){
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityExeption("Person", "id", String.valueOf(person.getId()));
        }
        personRepository.update(person);
    }

    @Override
    public void create(Person person) {

        boolean duplicateExists = true;
        try {
            personRepository.getById(person.getId());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityExeption("Person", "Id", String.valueOf(person.getId()));
        }

        personRepository.create(person);
    }


}
