package com.telerikacademy.web.deliverit.utils;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.dtos.DisplayCitiesDto;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CityMapper {

    private final CityService cityService;

    @Autowired
    public CityMapper(CityService cityService) {
        this.cityService = cityService;
    }

    public List<DisplayCitiesDto> cityFromDto(List<City> list) {
        List<DisplayCitiesDto> listCityDto = new ArrayList<>();
        for (City element : list) {
            DisplayCitiesDto dto = new DisplayCitiesDto();
            dto.setCityName(element.getName());
            dto.setCountryName(element.getCounty().getName());
            listCityDto.add(dto);
        }
        return listCityDto;
    }

    public DisplayCitiesDto cityFromDto(int id) {
        DisplayCitiesDto cityDto = new DisplayCitiesDto();
        cityDto.setCityName(cityService.getById(id).getName());
        cityDto.setCountryName(cityService.getById(id).getCounty().getName());
        return cityDto;
    }
}
