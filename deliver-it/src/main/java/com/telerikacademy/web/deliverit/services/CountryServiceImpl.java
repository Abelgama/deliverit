package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Country;
import com.telerikacademy.web.deliverit.repositoies.contracts.CountryRepository;
import com.telerikacademy.web.deliverit.services.contracts.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
public class CountryServiceImpl implements CountryService {

    private final CountryRepository countryRepository;

    @Autowired
    public CountryServiceImpl(CountryRepository repository) {
        this.countryRepository = repository;
    }

    @Override
    public List<Country> getAll(){
        return countryRepository.getAll();
    }

    @Override
    public Country getById(int id){
        try {
            return countryRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw  new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
