package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.Warehouse;

import java.util.List;

public interface WarehouseRepository {
    List<Warehouse> getAll();

    Warehouse getById(int id);

    void delete(int id);

    void update(Warehouse warehouse);

    void create(Warehouse warehouse);
}
