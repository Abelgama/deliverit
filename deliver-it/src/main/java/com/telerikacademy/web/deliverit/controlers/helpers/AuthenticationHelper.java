package com.telerikacademy.web.deliverit.controlers.helpers;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.services.contracts.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

@Component
public class AuthenticationHelper {
    private static final String AUTHORIZATION_HEADER_NAME ="Authorization";
    private final PersonService personService;

    @Autowired
    public AuthenticationHelper(PersonService personService) {
        this.personService = personService;
    }

    public Person tryGetPerson(HttpHeaders headers){
        if(!headers.containsKey(AUTHORIZATION_HEADER_NAME)){
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "The resourse requaried authentication");
        }
        try {
            String personEmail = headers.getFirst(AUTHORIZATION_HEADER_NAME);
            return personService.getByEmail(personEmail).stream().findFirst().get();
        } catch (EntityNotFoundException e){
            throw  new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Invalid User");
        }
    }
}
