package com.telerikacademy.web.deliverit.controlers.helpers;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Component
@EnableSwagger2
public class SwaggerConfigurationHelper {
    @Bean
    public Docket swaggerConfiguration() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .build()
                .apiInfo(applicationInfo());
    }

    private ApiInfo applicationInfo() {
        return new ApiInfo(
                "DeliverIT API",
                "Logistic Management System",
                "1.0",
                "",
                new springfox.documentation.service.Contact("Ivan Kolev and Abel Gama", "", "ivan.kolev.a30@learn.telerikacademy.com or abel.gama.a30@learn.telerikacademy.com"),
                "Telerik Academy ltd",
                "",
                Collections.emptyList());
    }
}
