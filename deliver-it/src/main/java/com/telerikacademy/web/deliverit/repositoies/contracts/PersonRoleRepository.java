package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.PersonRole;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public interface PersonRoleRepository {

    public List<PersonRole> getAll();

    public PersonRole getById(int id);
}
