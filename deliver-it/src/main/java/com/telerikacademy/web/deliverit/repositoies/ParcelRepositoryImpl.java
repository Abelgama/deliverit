package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ParcelRepositoryImpl implements ParcelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelRepositoryImpl(SessionFactory sessionFactory){
    this.sessionFactory = sessionFactory;

    }

    @Override
    public List<Parcel> getAll() {
        try (Session session = sessionFactory.openSession()) {
        Query<Parcel> query = session.createQuery(" from Parcel",Parcel.class);
        return query.list();
        }
    }

    @Override
    public Parcel getById(int id) {
        try (Session session = sessionFactory.openSession()){
            Parcel parcel = session.get(Parcel.class,id);
            if (parcel ==null){
                throw new EntityNotFoundException("Parcel",id);
            }
            return parcel;
        }
    }

    @Override
    public List<Parcel> getMyParcels(int statusId , String email) {
        try (Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("from Parcel p where p.shipment.shipmentStatus.id=:statusId and p.customer.email = :email",Parcel.class);
            query.setParameter("statusId", statusId);
            query.setParameter("email", email);
            return query.list();
        }
    }

    @Override
    public List<Parcel> getByWeight(int weight) {
        try (Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("from Parcel where weight = :weight",Parcel.class);
            query.setParameter("weight", weight);
            return query.list();
        }
    }

    @Override
    public List<Parcel> getByCustomer(int customerId) {
        try (Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("from Parcel where customer.id = :customerId",Parcel.class);
            query.setParameter("customerId", customerId);
            return query.list();
        }
    }

    @Override
    public List<Parcel> getByWarehouse(int warehouseId) {
        try (Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("from Parcel where warehouse.id = :warehouseId",Parcel.class);
            query.setParameter("warehouseId", warehouseId);
            return query.list();
        }
    }
    @Override
    public List<Parcel> getByCategory(int category) {
        try (Session session = sessionFactory.openSession()){
            Query<Parcel> query = session.createQuery("from Parcel where category.id = :category",Parcel.class);
            query.setParameter("category", category);
            return query.list();
        }
    }

    @Override
    public List<Parcel> getByMultipleCriteria(Optional<Integer> customerId,
                                              Optional<Integer> category) {

        String baseQuery = "from Parcel p ";
        if (customerId.isPresent()&& category.isPresent()){
            baseQuery +="where p.customer.id =:customerId and p.category.id=:category";
        }else if (customerId.isPresent()){
            baseQuery +="where p.customer.id =:customerId";
        }else if (category.isPresent()){
            baseQuery +="where p.category.id=:category";
        }

        try (Session session = sessionFactory.openSession()){

            Query<Parcel> query = session.createQuery(baseQuery,Parcel.class);

            if (customerId.isPresent()&& category.isPresent()){
                query.setParameter("customerId", customerId.orElse(0));
                query.setParameter("category", category.orElse(0));
            }else if (customerId.isPresent()){
                query.setParameter("customerId", customerId.orElse(0));
            }else if (category.isPresent()){
                query.setParameter("category", category.orElse(0));
            }

            return query.list();
        }
    }

    @Override
    public void delete(int id) {
        Parcel parcelToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(parcelToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(parcel);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(Parcel parcel) {
        try (Session session = sessionFactory.openSession()) {
            session.save(parcel);
        }
    }
}
