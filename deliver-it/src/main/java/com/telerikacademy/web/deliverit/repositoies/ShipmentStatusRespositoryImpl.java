package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ShipmentStatus;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentStatusRespository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ShipmentStatusRespositoryImpl implements ShipmentStatusRespository {

    private final SessionFactory sessionFactory;

    public ShipmentStatusRespositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ShipmentStatus> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ShipmentStatus> queue = session.createQuery("from ShipmentStatus ", ShipmentStatus.class);
            return queue.list();
        }
    }

    @Override
    public ShipmentStatus getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ShipmentStatus status = session.get(ShipmentStatus.class, id);
            if (status == null) {
                throw new EntityNotFoundException("City", id);
            }
            return status;
        }
    }
}
