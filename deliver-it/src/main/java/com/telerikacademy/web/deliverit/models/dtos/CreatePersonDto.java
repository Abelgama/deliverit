package com.telerikacademy.web.deliverit.models.dtos;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.PersonRole;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CreatePersonDto {

    @NotBlank(message = "First name can't be empty")
    @Size(min = 3, max = 20, message = "First name should be between 3 and 20 symbols")
    private String firstName;


    @NotBlank(message = "Last name can't be empty")
    @Size(min = 3, max = 20, message = "Last name should be between 3 and 20 symbols")
    private String lastName;


    @NotBlank(message = "E-mail can't be empty")
    @Size(min = 8, max = 40, message = "E-mail should be between 8 and 40 symbols")
    private String email;


    @NotBlank(message = "City can't be empty")
    private int cityId;


    @NotBlank(message = "Street name can't be empty")
    @Size(min = 3, max = 30, message = "Street name should be between 3 and 30 symbols")
    private String streetName;

    @NotBlank(message = "Role can't be empty")
    private int roleId;

    public CreatePersonDto() {
    }

    public CreatePersonDto(int id, String firstName, String lastName, String email, int cityId, String streetName, int roleId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.cityId = cityId;
        this.streetName = streetName;
        this.roleId = roleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String customer) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String warehouse) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String category) {
        this.email = email;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }
}
