package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ParcelCategory;
import com.telerikacademy.web.deliverit.models.ShipmentStatus;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelCategoryRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class ParcelCategoryRepositoryImpl implements ParcelCategoryRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelCategoryRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ParcelCategory> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ParcelCategory> queue = session.createQuery("from ParcelCategory ", ParcelCategory.class);
            return queue.list();
        }
    }

    @Override
    public ParcelCategory getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ParcelCategory category = session.get(ParcelCategory.class, id);
            if (category == null) {
                throw new EntityNotFoundException("Category", id);
            }
            return category;
        }
    }
}
