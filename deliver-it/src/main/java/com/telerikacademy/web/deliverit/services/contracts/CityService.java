package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.City;

import java.util.List;

public interface CityService {
    List<City> getAll();

    City getById(int id);

    City getByName(String name);
}
