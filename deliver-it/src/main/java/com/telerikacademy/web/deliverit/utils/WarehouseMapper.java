package com.telerikacademy.web.deliverit.utils;

import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.CreateWarehouseDto;
import com.telerikacademy.web.deliverit.models.dtos.DisplayCitiesDto;
import com.telerikacademy.web.deliverit.models.dtos.DisplayWarehousesDto;
import com.telerikacademy.web.deliverit.repositoies.contracts.CityRepository;
import com.telerikacademy.web.deliverit.repositoies.contracts.WarehouseRepository;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentStatusService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WarehouseMapper {

    private final WarehouseService warehouseService;
    private final WarehouseRepository warehouseRepository;
    private final ShipmentStatusService shipmentStatusService;
    private final CityService cityService;
    private final CityRepository cityRepository;

    @Autowired
    public WarehouseMapper(WarehouseService warehouseService, WarehouseRepository warehouseRepository, ShipmentStatusService shipmentStatusService, CityService cityService, CityRepository cityRepository) {
        this.warehouseService = warehouseService;
        this.warehouseRepository = warehouseRepository;
        this.shipmentStatusService = shipmentStatusService;
        this.cityService = cityService;
        this.cityRepository = cityRepository;
    }

    public DisplayWarehousesDto warehouseFromDto(int id) {
        DisplayWarehousesDto warehouseDto = new DisplayWarehousesDto();
        warehouseDto.setCountry(warehouseService.getById(id).getCity().getCounty().getName());
        warehouseDto.setCity(warehouseService.getById(id).getCity().getName());
        warehouseDto.setAddress(warehouseService.getById(id).getName());
        return warehouseDto;
    }

    public Warehouse warehouseFromDto(CreateWarehouseDto dto) {
        Warehouse warehouseToCreate = new Warehouse();
        warehouseToCreate.setCity(cityService.getById(dto.getCityId()));
        warehouseToCreate.setName(dto.getName());
        return warehouseToCreate;
    }

    public Warehouse warehouseFromDto(CreateWarehouseDto dto, int id) {
        Warehouse warehouseToUpdate = warehouseRepository.getById(id);
        dtoToObject(dto, warehouseToUpdate);
        return warehouseToUpdate;
    }


    private void dtoToObject(CreateWarehouseDto warehouseDto, Warehouse warehouse) {
        City city = cityRepository.getById(warehouseDto.getCityId());
        warehouse.setCity(city);
        warehouse.setName(warehouseDto.getName());
    }

    public List<DisplayWarehousesDto> allWarehouseFromDto(List<Warehouse> list) {
        List<DisplayWarehousesDto> listWarehouseDto = new ArrayList<>();
        for (Warehouse element : list) {
            DisplayWarehousesDto dto = new DisplayWarehousesDto();
            dto.setCity(element.getCity().getName());
            dto.setCountry(element.getCity().getCounty().getName());
            dto.setAddress(element.getName());
            System.out.println();
            listWarehouseDto.add(dto);
        }
        return listWarehouseDto;
    }
}
