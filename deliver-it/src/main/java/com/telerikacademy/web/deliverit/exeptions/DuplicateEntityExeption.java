package com.telerikacademy.web.deliverit.exeptions;

public class DuplicateEntityExeption extends RuntimeException{
    public DuplicateEntityExeption(String type, String attribute, String value) {
        super(String.format("%s with %s %s already exists.", type, attribute, value));
    }
}
