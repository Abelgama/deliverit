package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
@Table(name="warehouses")
public class Warehouse {

    @Id
    @Column(name="warehouse_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    @JoinColumn (name="city_id")
    @NotNull(message = "City can not be empty")
    private City city;

    @Column(name = "street_name")
    @NotBlank(message = "Name can't be empty")
    @Size(min=3, max=20, message = "Name should be between 2 and 20 symbols")
    private String name;

    public Warehouse() {
    }

    public Warehouse(int id, City city, String name) {
        setId(id);
        setCity(city);
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
