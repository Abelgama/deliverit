package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ShipmentService {
    List<Shipment> getAll();

    Shipment getById(int id);

    void delete(int id);

    List<Shipment> getMyShipmentStatus(String email,Person person);

    List<Shipment> getNextShipmentToArrive(int id,Person person);

    void update(Shipment shipment, Person person);

    void create(Shipment shipment, Person person);

    List<Shipment> filter(Optional <Integer> id);
}
