package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ParcelDeliveryOption;
import com.telerikacademy.web.deliverit.models.PersonRole;
import com.telerikacademy.web.deliverit.repositoies.contracts.PersonRoleRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class PersonRoleRepositoryIml implements PersonRoleRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonRoleRepositoryIml(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<PersonRole> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<PersonRole> queue = session.createQuery("from PersonRole ", PersonRole.class);
            return queue.list();
        }
    }

    @Override
    public PersonRole getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            PersonRole role = session.get(PersonRole.class, id);
            if (role == null) {
                throw new EntityNotFoundException("role", id);
            }
            return role;
        }
    }
}
