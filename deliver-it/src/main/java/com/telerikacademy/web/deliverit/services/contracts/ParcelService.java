package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ParcelService {
    List<Parcel> getAll();

    Parcel getById(int id);

    void delete(int id);

    List<Parcel> getByWeight(int weight);

    List<Parcel> getByCustomer(int customerId);

    List<Parcel> getByWarehouse(int warehouseId);

    List<Parcel> getByCategory(int category);

    List<Parcel> getByMultipleCriteria(Optional<Integer> customerId,
                                       Optional<Integer> category);

    List<Parcel> getMyParcels(int statusId , String email,Person person);

    void update(Parcel parcel, Person person);

    void create(Parcel parcel,Person person);
}
