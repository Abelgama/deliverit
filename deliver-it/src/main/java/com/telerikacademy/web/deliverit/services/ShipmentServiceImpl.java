package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.exeptions.InvalidDateException;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class ShipmentServiceImpl implements ShipmentService {
    private final ShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentServiceImpl(ShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }


    @Override
    public List<Shipment> getAll() {
        return shipmentRepository.getAll();
    }

    @Override
    public Shipment getById(int id) {
        try {
            return shipmentRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        shipmentRepository.delete(id);
    }

    @Override
    public List<Shipment> getMyShipmentStatus(String email,Person person) {
        if (!person.getEmail().equals(email)) {
            throw new UnsupportedOperationException("Only parcel owner can check his shipment status");
        }
        return shipmentRepository.getMyShipmentStatus(email);
    }

    @Override
    public List<Shipment> getNextShipmentToArrive(int id,Person person) {
        if (!person.isEmployee()) {
            throw new UnsupportedOperationException("Only employee can check shipment");
        }
        return shipmentRepository.getNextShipmentToArrive(id);
    }

    @Override
    public void update(Shipment shipment, Person person) {
        if (!person.isEmployee()) {
            throw new UnsupportedOperationException("Only employee can create shipment");
        }
        shipmentRepository.update(shipment);
    }

    @Override
    public void create(Shipment shipment, Person person) {
        if (!person.isEmployee()) {
            throw new UnsupportedOperationException("Only employee can create shipment");
        }
        boolean duplicateExist = true;
        try {
            Shipment existingShipment = shipmentRepository.getById(shipment.getId());
            if (existingShipment.getId() == shipment.getId()) {
                duplicateExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        if (duplicateExist) {
            throw new DuplicateEntityExeption("Shipment", "Id", shipment.getId() + "");
        }
        if(shipment.getDateOfArrival().isBefore(shipment.getDateOfDeparture())){
            throw new InvalidDateException("Arrival date is before Departure");
        }
        if(shipment.getDateOfArrival().isBefore(LocalDate.now())){
            throw new InvalidDateException("Arrival date can't be in the past");
        }
        shipmentRepository.create(shipment);
    }


    public List<Shipment> filter(Optional<Integer> id) {
        return shipmentRepository.filter(id);
    }
}
