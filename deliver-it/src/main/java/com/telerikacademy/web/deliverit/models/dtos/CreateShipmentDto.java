package com.telerikacademy.web.deliverit.models.dtos;

import org.apache.tomcat.jni.Local;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class CreateShipmentDto {

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String departureDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private String arrivalDate;
    @Positive (message = "Origin warehouse Id should be positive")
    private int originWarehouse;
    @Positive (message = "Destination warehouse Id should be positive")
    private int destinationWarehouse;
    @Positive (message = "Shipment status can't be empty")
    private int shipmentStatus;

    public CreateShipmentDto() {
    }

    public CreateShipmentDto(String departureDate, String arrivalDate, int originWarehouse, int destinationWarehouse, int shipmentStatus) {
        setArrivalDate(arrivalDate);
        setDepartureDate(departureDate);
        setOriginWarehouse(originWarehouse);
        setDestinationWarehouse(destinationWarehouse);
        setShipmentStatus(shipmentStatus);
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public int getOriginWarehouse() {
        return originWarehouse;
    }

    public void setOriginWarehouse(int originWarehouse) {
        this.originWarehouse = originWarehouse;
    }

    public int getDestinationWarehouse() {
        return destinationWarehouse;
    }

    public void setDestinationWarehouse(int destinationWarehouse) {
        this.destinationWarehouse = destinationWarehouse;
    }

    public int getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(int shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }
}
