package com.telerikacademy.web.deliverit.controlers;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.dtos.CreateParcelsDto;
import com.telerikacademy.web.deliverit.models.dtos.CreatePersonDto;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.services.contracts.PersonService;
import com.telerikacademy.web.deliverit.utils.ParcelMapper;
import com.telerikacademy.web.deliverit.utils.PersonMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api/persons")
public class PersonController {
    private final PersonService personService;
    private final PersonMapper mapper;

    public PersonController(PersonService personService,PersonMapper mapper) {
        this.personService = personService;
        this.mapper = mapper;

    }

    @GetMapping
    public List<Person> getAll() {
        return personService.getAll();
    }

    @GetMapping("/{id}")
    public Person getById(@PathVariable int id) {
        try {
            return personService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<Person> getByEmail(@RequestParam String email) {
        try {
            return personService.getByEmail(email);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/search")
    public List<Person> searchByCustomerName(@RequestParam (required = false) String firstName,
                                              @RequestParam (required = false) String lastName) {
        try {
            return personService.searchByCustomerName(Optional.ofNullable(firstName),Optional.ofNullable(lastName));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            personService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Person update(@PathVariable int id, @Valid @RequestBody CreatePersonDto personDto) {
        try {
            Person person = mapper.personFromDto(personDto,id);
            personService.update(person);
            return person;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityExeption e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping()
    public Person create(@Valid @RequestBody CreatePersonDto personDto) {
        try {
            Person person = mapper.personFromDto(personDto);
            personService.create(person);
            return person;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityExeption e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }
}


