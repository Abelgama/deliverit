package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class DisplayCitiesDto {

    @NotBlank(message = "City can't be empty")
    @Size(min=3, max=20, message = "Name should be between 3 and 20 symbols")
    private String cityName;
    @NotBlank(message = "Country can't be empty")
    @Size(min=3, max=20, message = "Name should be between 3 and 20 symbols")
    private String countryName;

    public DisplayCitiesDto() {
    }

    public DisplayCitiesDto(String cityName, String countryName) {
        this.cityName = cityName;
        this.countryName = countryName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
