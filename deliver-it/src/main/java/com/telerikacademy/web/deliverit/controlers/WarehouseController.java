package com.telerikacademy.web.deliverit.controlers;

import com.telerikacademy.web.deliverit.controlers.helpers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.models.dtos.CreateWarehouseDto;
import com.telerikacademy.web.deliverit.models.dtos.DisplayWarehousesDto;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import com.telerikacademy.web.deliverit.utils.WarehouseMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/api/warehouses")
public class WarehouseController {

    private final WarehouseService warehouseService;
    private final WarehouseMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    public WarehouseController(WarehouseService warehouseService, WarehouseMapper mapper, AuthenticationHelper authenticationHelper) {
        this.warehouseService = warehouseService;
        this.mapper = mapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<DisplayWarehousesDto> getAll() {
        return mapper.allWarehouseFromDto(warehouseService.getAll());
    }

    @GetMapping("/{id}")
    public DisplayWarehousesDto getById(@PathVariable int id) {
        try {
            return mapper.warehouseFromDto(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders header, @PathVariable int id) {
        try {
            authenticationHelper.tryGetPerson(header);
            warehouseService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public void update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody CreateWarehouseDto dto) {
        try {
            Person person = authenticationHelper.tryGetPerson(headers);
            Warehouse warehouseToUpdate = mapper.warehouseFromDto(dto, id);
            warehouseService.update(warehouseToUpdate, person);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public boolean create(@RequestHeader HttpHeaders header, @Valid @RequestBody CreateWarehouseDto dto) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            Warehouse warehouseToCreate = mapper.warehouseFromDto(dto);
            warehouseService.create(warehouseToCreate, person);
            return true;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityExeption e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
