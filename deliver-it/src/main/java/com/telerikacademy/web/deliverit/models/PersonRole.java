package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
@Table(name = "roles" )
public class PersonRole {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="role_id")
    @Positive(message = "Id should be positive")
    private int id;

    @Column(name="name")
    @NotBlank(message = "Status can't be empty")
    @Size(min=3, max=20, message = "Status should be between 3 and 20 symbols")
    private String name;

    public PersonRole() {
    }

    public PersonRole(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
