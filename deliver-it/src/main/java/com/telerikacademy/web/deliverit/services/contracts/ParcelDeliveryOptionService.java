package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.ParcelDeliveryOption;

import java.util.List;

public interface ParcelDeliveryOptionService {

    List<ParcelDeliveryOption> getAll();

    ParcelDeliveryOption getById(int id);

}
