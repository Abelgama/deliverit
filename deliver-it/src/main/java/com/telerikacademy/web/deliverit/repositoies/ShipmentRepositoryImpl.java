package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public class ShipmentRepositoryImpl implements ShipmentRepository {

    private final SessionFactory sessionFactory;

    public ShipmentRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Shipment> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment", Shipment.class);
            return query.list();
        }
    }

    @Override
    public Shipment getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Shipment shipment = session.get(Shipment.class, id);
            if (shipment == null) {
                throw new EntityNotFoundException("Shipment", id);
            }
            return shipment;
        }
    }

    // Private part: Customers should have to ability to see the status of the shipment that holds a given parcel of theirs.
    @Override
    public List<Shipment> getMyShipmentStatus(String email) {
        try (Session session = sessionFactory.openSession()){
            Query<Shipment> query = session.createQuery("from Shipment s where s.id in (select shipment.id from Parcel p where p.customer.email=:email )",Shipment.class);
            query.setParameter("email", email);
            return query.list();
        }
    }

    // Admin part: Employees should be able to see which the next shipment is to arrive for a given warehouse.
    @Override
    public List<Shipment> getNextShipmentToArrive(int id) {
        LocalDate now = LocalDate.now();
        try (Session session = sessionFactory.openSession()){
            Query<Shipment> query = session.createQuery("from Shipment s where s.dateOfArrival>:now and s.destinationWarehouse.id = :id  order by s.dateOfArrival ASC",Shipment.class);
            query.setParameter("now", now);
            query.setParameter("id", id);
            query.setMaxResults(1);
            return query.list();
        }
    }

    @Override
    public void delete(int id) {
        Shipment shipmentToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(shipmentToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(shipment);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.save(shipment);
        }
    }

    @Override
    public List<Shipment> filter(Optional <Integer> id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Shipment> query = session.createQuery("from Shipment where originWarehouse.id = :id", Shipment.class);
            query.setParameter("id", id.orElse(0));
            return query.list();
        }
    }
}
