package com.telerikacademy.web.deliverit.utils;

import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.models.dtos.CreateParcelsDto;
import com.telerikacademy.web.deliverit.repositoies.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ParcelMapper {
    private final ParcelRepository parcelRepository;
    private final PersonRepository personRepository;
    private final ShipmentRepository shipmentRepository;
    private final WarehouseRepository warehouseRepository;
    private final ParcelCategoryRepository categoryRepository;
    private final ParcelDeliveryOptionRepository deliveryRepository;

    @Autowired
    public ParcelMapper( ParcelRepository parcelRepository ,PersonRepository personRepository, ShipmentRepository shipmentRepository,
                        WarehouseRepository warehouseRepository,ParcelCategoryRepository categoryRepository,
                        ParcelDeliveryOptionRepository deliveryRepository){
        this.parcelRepository = parcelRepository;
        this.personRepository = personRepository;
        this.shipmentRepository = shipmentRepository;
        this.warehouseRepository = warehouseRepository;
        this.categoryRepository = categoryRepository;
        this.deliveryRepository = deliveryRepository;
    }

    public Parcel parcelFromDto(CreateParcelsDto parcelDto) {
        Parcel  parcel  = new Parcel();
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    public Parcel parcelFromDto(CreateParcelsDto parcelDto, int id) {
        Parcel parcel  = parcelRepository.getById(id);
        dtoToObject(parcelDto, parcel);
        return parcel;
    }

    private void dtoToObject(CreateParcelsDto parcelDto, Parcel parcel ) {
       Person person = personRepository.getById(parcelDto.getCustomerId());
       parcel.setCustomer(person);

        Shipment shipment = shipmentRepository.getById(parcelDto.getShipmentId());
        parcel.setShipment(shipment);

        Warehouse warehouse = warehouseRepository.getById(parcelDto.getWarehouseId());
        parcel.setWarehouse(warehouse);

        ParcelCategory category = categoryRepository.getById(parcelDto.getCategoryId());
        parcel.setCategory(category);

        ParcelDeliveryOption option = deliveryRepository.getById(parcelDto.getDeliveryOptionId());
        parcel.setDeliveryOption(option);

        parcel.setWeight(parcelDto.getWeight());

    }

}

