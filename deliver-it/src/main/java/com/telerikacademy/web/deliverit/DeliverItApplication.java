package com.telerikacademy.web.deliverit;

import com.telerikacademy.web.deliverit.controlers.helpers.SwaggerConfigurationHelper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeliverItApplication {

    public static void main(String[] args) {
        SpringApplication.run(DeliverItApplication.class, args);
        SwaggerConfigurationHelper swaggerConfiguration = new SwaggerConfigurationHelper();
        swaggerConfiguration.swaggerConfiguration();
    }
}
