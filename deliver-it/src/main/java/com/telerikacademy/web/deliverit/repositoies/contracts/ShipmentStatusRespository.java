package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.ShipmentStatus;

import java.util.List;

public interface ShipmentStatusRespository {
    List<ShipmentStatus> getAll();

    ShipmentStatus getById(int id);
}
