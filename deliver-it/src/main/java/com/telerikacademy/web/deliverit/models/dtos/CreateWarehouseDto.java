package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class CreateWarehouseDto {

    @Positive(message = "City Id should be positive")
    private int cityId;

    @NotBlank(message = "Name can't be empty")
    @Size(min=3, max=20, message = "Name should be between 3 and 20 symbols")
    private String name;

    public CreateWarehouseDto() {
    }

    public CreateWarehouseDto( int cityId, String name) {

        this.cityId = cityId;
        this.name = name;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
