package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.ParcelCategory;

import java.util.List;

public interface ParcelCategoryService {

    List<ParcelCategory> getAll();

    ParcelCategory getById(int id);


}
