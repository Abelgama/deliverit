package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.ShipmentStatus;

import javax.persistence.Entity;
import java.util.List;

public interface ShipmentStatusService {
    List<ShipmentStatus> getAll();

    ShipmentStatus getById(int id);
}
