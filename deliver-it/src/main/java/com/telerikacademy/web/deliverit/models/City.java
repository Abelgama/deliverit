package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
@Table(name="cities")
public class City {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="city_id")
    private int id;

    @OneToOne
    @JoinColumn (name="country_id")
    @NotBlank(message = "County can't be empty")
    private Country county;

    @Column(name="name")
    @NotBlank(message = "Name can't be empty")
    @Size(min=3, max=20, message = "Name should be between 2 and 20 symbols")
    private String name;

    public City() {
    }

    public City(int id, Country county, String name) {
        setId(id);
        setCounty(county);
        setName(name);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Country  getCounty() {
        return county;
    }

    public void setCounty(Country county) {
        this.county = county;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
