package com.telerikacademy.web.deliverit.models.dtos;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;


public class DisplayShipmentsDto {

    @Positive(message = "Id should be positive")
    private int id;

    @NotBlank(message = "Date of Departure can not be empty")
    private String dateOfDeparture;

    @NotBlank(message = "Date of Arrival can not be empty")
    private String dateOfArrival;

    @NotBlank(message = "Status can not be empty")
    private String shipmentStatus;

    @NotBlank (message = "Warehouse of origin can not be empty")
    private String originWarehouse;

    @NotBlank (message = "Warehouse of destination can not be empty")
    private String destinationWarehouse;

    public DisplayShipmentsDto() {
    }

    public DisplayShipmentsDto(int id, String dateOfDeparture, String dateOfArrival, String shipmentStatus, String originWarehouse, String destinationWarehouse) {
        this.id = id;
        this.dateOfDeparture = dateOfDeparture;
        this.dateOfArrival = dateOfArrival;
        this.shipmentStatus = shipmentStatus;
        this.originWarehouse = originWarehouse;
        this.destinationWarehouse = destinationWarehouse;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(String dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    public String getDateOfArrival() {
        return dateOfArrival;
    }

    public void setDateOfArrival(String dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    public String getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(String shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public String getOriginWarehouse() {
        return originWarehouse;
    }

    public void setOriginWarehouse(String originWarehouse) {
        this.originWarehouse = originWarehouse;
    }

    public String getDestinationWarehouse() {
        return destinationWarehouse;
    }

    public void setDestinationWarehouse(String destinationWarehouse) {
        this.destinationWarehouse = destinationWarehouse;
    }
}
