package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelRepository;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Component
public class ParcelServiceImpl implements ParcelService {

    private final ParcelRepository parcelRepository;

        @Autowired
        public ParcelServiceImpl(ParcelRepository parcelRepository) {
            this.parcelRepository = parcelRepository;
        }

        @Override
        public List<Parcel> getAll() {
            return parcelRepository.getAll();
        }

        @Override
        public Parcel getById(int id) {
            try {
                return parcelRepository.getById(id);
            } catch (EntityNotFoundException e) {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
            }
        }

    @Override
    public List<Parcel> getMyParcels(int statusId,String email,Person person) {
        if (!person.getEmail().equals(email)) {
            throw new UnsupportedOperationException("Only parcel owner can check his parcels");
        }
        return parcelRepository.getMyParcels(statusId,email);
    }

    @Override
    public List<Parcel> getByWeight(int weight) {
        return parcelRepository.getByWeight(weight);
    }

    @Override
    public List<Parcel> getByCustomer(int customerId) {
        return parcelRepository.getByCustomer(customerId);
    }

    @Override
    public List<Parcel> getByWarehouse(int warehouseId) {
        return parcelRepository.getByWarehouse(warehouseId);
    }

    @Override
    public List<Parcel> getByCategory(int category) {
        return parcelRepository.getByCategory(category);
    }

    @Override
    public List<Parcel> getByMultipleCriteria(Optional<Integer> customerId, Optional<Integer> category) {
        return parcelRepository.getByMultipleCriteria(customerId,category);
    }

    @Override
        public void delete(int id) {
            parcelRepository.delete(id);
        }

        @Override
        public void update(Parcel parcel, Person person) {
            if (!person.isEmployee()) {
                throw new UnsupportedOperationException("Only employee can update shipment");
            }
            boolean duplicateExists = true;
            try {
                Parcel existingParcel = parcelRepository.getById(parcel.getId());
                if (existingParcel.getId() == parcel.getId()){
                    duplicateExists = false;
                }
            } catch (EntityNotFoundException e) {
                duplicateExists = false;
            }

            if (duplicateExists) {
                throw new DuplicateEntityExeption("Parcel", "id", String.valueOf(parcel.getId()));
            }

            parcelRepository.update(parcel);
        }

        @Override
        public void create(Parcel parcel,Person person) {
            if (!person.isEmployee()) {
                throw new UnsupportedOperationException("Only employee can update shipment");
            }
            boolean duplicateExists = true;
            try {
                parcelRepository.getById(parcel.getId());
            } catch (EntityNotFoundException e) {
                duplicateExists = false;
            }

            if (duplicateExists) {
                throw new DuplicateEntityExeption("Parcel", "Id", String.valueOf(parcel.getId()));
            }
            parcelRepository.create(parcel);
        }


}
