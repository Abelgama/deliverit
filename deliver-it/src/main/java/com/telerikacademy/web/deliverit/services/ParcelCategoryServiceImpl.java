package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ParcelCategory;
import com.telerikacademy.web.deliverit.models.ParcelDeliveryOption;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelCategoryRepository;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelDeliveryOptionRepository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
public class ParcelCategoryServiceImpl implements ParcelCategoryService {

    private final ParcelCategoryRepository categoryRepository;

    @Autowired
    public ParcelCategoryServiceImpl(ParcelCategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<ParcelCategory> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public ParcelCategory getById(int id) {
        try {
            return categoryRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
