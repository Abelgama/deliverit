package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ParcelCategory;
import com.telerikacademy.web.deliverit.models.ParcelDeliveryOption;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelDeliveryOptionRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class ParcelDeliveryOptionRepositoryImpl implements ParcelDeliveryOptionRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public ParcelDeliveryOptionRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ParcelDeliveryOption> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<ParcelDeliveryOption> queue = session.createQuery("from ParcelDeliveryOption ", ParcelDeliveryOption.class);
            return queue.list();
        }
    }

    @Override
    public ParcelDeliveryOption getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            ParcelDeliveryOption option = session.get(ParcelDeliveryOption.class, id);
            if (option == null) {
                throw new EntityNotFoundException("Option", id);
            }
            return option;
        }
    }
}
