package com.telerikacademy.web.deliverit.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Entity
@Table(name = "persons")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_id")
    @Positive(message = "Id should be positive")
    private int id;

    @Column(name = "first_name")
    @NotBlank(message = "First name can't be empty")
    @Size(min = 3, max = 20, message = "First name should be between 3 and 20 symbols")
    private String firstName;

    @Column(name = "last_name")
    @NotBlank(message = "Last name can't be empty")
    @Size(min = 3, max = 20, message = "Last name should be between 3 and 20 symbols")
    private String lastName;

    @Column(name = "email")
    @NotBlank(message = "E-mail can't be empty")
    @Size(min = 8, max = 40, message = "E-mail should be between 8 and 40 symbols")
    private String email;

    @OneToOne
    @JoinColumn(name = "city_id")
    @NotBlank(message = "City can't be empty")
    private City city;

    @Column(name = "street_name")
    @NotBlank(message = "Street name can't be empty")
    @Size(min = 3, max = 30, message = "Street name should be between 3 and 30 symbols")
    private String streetName;

    @OneToOne
    @JoinColumn(name = "person_role")
    private PersonRole role;

    public Person() {

    }

    public Person(int id, String firstName, String lastName, String email, City city, String streetName, PersonRole role) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.city = city;
        this.streetName = streetName;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String customer) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String warehouse) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String category) {
        this.email = email;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public PersonRole getRole() {
        return role;
    }

    public void setRole(PersonRole role) {
        this.role = role;
    }

    public boolean isEmployee() {
        if (getRole().getId() == 1) {
            return true;
        }
        return false;
    }
}
