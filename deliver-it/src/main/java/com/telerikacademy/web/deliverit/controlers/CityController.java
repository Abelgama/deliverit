package com.telerikacademy.web.deliverit.controlers;

import com.telerikacademy.web.deliverit.models.dtos.DisplayCitiesDto;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import com.telerikacademy.web.deliverit.utils.CityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/api/cities")
public class CityController {

    private final CityService cityService;
    private final CityMapper mapper;

    @Autowired
    public CityController(CityService service, CityMapper mapper) {
        this.cityService = service;
        this.mapper = mapper;
    }

    @RequestMapping
    public List<DisplayCitiesDto> getAll() {
        //return mapper.fromDto(cityService.getAll());
        return mapper.cityFromDto(cityService.getAll());
    }

    @RequestMapping("/{id}")
    public DisplayCitiesDto getById(@PathVariable int id){
        return mapper.cityFromDto(id);
    }
}
