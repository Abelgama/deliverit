package com.telerikacademy.web.deliverit.models;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "shipments")
public class Shipment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shipment_id")
    private int id;

    @Column(name = "departure_date")
    private LocalDate dateOfDeparture;

    @Column(name = "arrival_date")
    private LocalDate dateOfArrival;

    @OneToOne
    @JoinColumn(name = "shipment_status")
    private ShipmentStatus shipmentStatus;

    @OneToOne
    @JoinColumn(name = "origin_warehouse_id")
    private Warehouse originWarehouse;

    @OneToOne
    @JoinColumn(name = "destination_warehouse_id")
    private Warehouse destinationWarehouse;

    public Shipment() {
    }

    public Shipment(int id, LocalDate dateOfDeparture, LocalDate dateOfArrival, ShipmentStatus shipmentStatus, Warehouse originWarehouse, Warehouse destinationWarehouse) {
        setId(id);
        setDateOfDeparture(dateOfDeparture);
        setDateOfArrival(dateOfArrival);
        setShipmentStatus(shipmentStatus);
        setOriginWarehouse(originWarehouse);
        setDestinationWarehouse(destinationWarehouse);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDateOfDeparture() {
        return dateOfDeparture;
    }

    public void setDateOfDeparture(LocalDate dateOfDeparture) {
        this.dateOfDeparture = dateOfDeparture;
    }

    public LocalDate getDateOfArrival() {
        return dateOfArrival;
    }

    public void setDateOfArrival(LocalDate dateOfArrival) {
        this.dateOfArrival = dateOfArrival;
    }

    public ShipmentStatus getShipmentStatus() {
        return shipmentStatus;
    }

    public void setShipmentStatus(ShipmentStatus shipmentStatus) {
        this.shipmentStatus = shipmentStatus;
    }

    public Warehouse getOriginWarehouse() {
        return originWarehouse;
    }

    public void setOriginWarehouse(Warehouse originWarehouse) {
        this.originWarehouse = originWarehouse;
    }

    public Warehouse getDestinationWarehouse() {
        return destinationWarehouse;
    }

    public void setDestinationWarehouse(Warehouse destinationWarehouse) {
        this.destinationWarehouse = destinationWarehouse;
    }
}
