package com.telerikacademy.web.deliverit.utils;

import com.telerikacademy.web.deliverit.models.Shipment;
import com.telerikacademy.web.deliverit.models.dtos.CreateShipmentDto;
import com.telerikacademy.web.deliverit.models.dtos.DisplayShipmentsDto;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentRepository;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentStatusRespository;
import com.telerikacademy.web.deliverit.repositoies.contracts.WarehouseRepository;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentService;
import com.telerikacademy.web.deliverit.services.contracts.ShipmentStatusService;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Component
public class ShipmentMapper {

    private final ShipmentService shipmentService;
    private final ShipmentRepository shipmentRepository;
    private final ShipmentStatusService shipmentStatusService;
    private final WarehouseService warehouseService;
    private final WarehouseRepository warehouseRepository;
    private final ShipmentStatusRespository shipmentStatusRespository;


    @Autowired
    public ShipmentMapper(ShipmentService shipmentService, ShipmentRepository shipmentRepository, ShipmentStatusService shipmentStatusService, WarehouseService warehouseService, WarehouseRepository warehouseRepository, ShipmentStatusRespository shipmentStatusRespository) {
        this.shipmentService = shipmentService;
        this.shipmentRepository = shipmentRepository;
        this.shipmentStatusService = shipmentStatusService;
        this.warehouseService = warehouseService;
        this.warehouseRepository = warehouseRepository;
        this.shipmentStatusRespository = shipmentStatusRespository;
    }

    public List<DisplayShipmentsDto> allShipmentsFromDto(List<Shipment> list) {
        List<DisplayShipmentsDto> listShipmentDto = new ArrayList<>();
        for (Shipment element : list) {
            DisplayShipmentsDto dto = new DisplayShipmentsDto();
            dto.setShipmentStatus(element.getShipmentStatus().getName());
            dto.setId(element.getId());
            dto.setDateOfDeparture(element.getDateOfDeparture() + "");
            dto.setDateOfArrival(element.getDateOfArrival() + "");
            dto.setOriginWarehouse(element.getOriginWarehouse().getId() + "");
            dto.setDestinationWarehouse(element.getDestinationWarehouse().getId() + "");
            listShipmentDto.add(dto);
        }
        return listShipmentDto;
    }

    public DisplayShipmentsDto shipmentsFromDto(int id) {
        DisplayShipmentsDto shipmentsDto = new DisplayShipmentsDto();
        shipmentsDto.setId(shipmentService.getById(id).getId());
        shipmentsDto.setDateOfDeparture(shipmentService.getById(id).getDateOfDeparture().toString());
        shipmentsDto.setDateOfArrival(shipmentService.getById(id).getDateOfArrival().toString());
        shipmentsDto.setShipmentStatus(shipmentService.getById(id).getShipmentStatus().getName() + "");
        shipmentsDto.setOriginWarehouse(shipmentService.getById(id).getOriginWarehouse().getId() + "");
        shipmentsDto.setDestinationWarehouse(shipmentService.getById(id).getDestinationWarehouse().getId() + "");
        return shipmentsDto;
    }

    public Shipment shipmentFromDto(CreateShipmentDto dto) {
        Shipment shipmentToCreate = new Shipment();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        shipmentToCreate.setShipmentStatus(shipmentStatusService.getById(dto.getShipmentStatus()));
        shipmentToCreate.setOriginWarehouse(warehouseService.getById(dto.getOriginWarehouse()));
        shipmentToCreate.setDestinationWarehouse(warehouseService.getById(dto.getDestinationWarehouse()));
        if (dto.getArrivalDate() == null || dto.getArrivalDate().isBlank()) {
            dto.setArrivalDate("");
        } else {
            shipmentToCreate.setDateOfArrival(LocalDate.parse(dto.getArrivalDate(), formatter));
        }
        if (dto.getDepartureDate() == null || dto.getDepartureDate().isBlank()) {
            dto.setDepartureDate("");
        } else {
            shipmentToCreate.setDateOfDeparture(LocalDate.parse(dto.getDepartureDate(), formatter));
        }
        return shipmentToCreate;
    }

    public Shipment shipmentFromDto(CreateShipmentDto dto, int id) {
        Shipment shipmentToUpdate = shipmentRepository.getById(id);
        dtoToObject(dto, shipmentToUpdate);
        return shipmentToUpdate;
    }

    private void dtoToObject(CreateShipmentDto shipmentDto, Shipment shipment) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        shipment.setShipmentStatus(shipmentStatusRespository.getById(shipmentDto.getShipmentStatus()));
        shipment.setOriginWarehouse(warehouseRepository.getById(shipmentDto.getOriginWarehouse()));
        shipment.setDestinationWarehouse(warehouseRepository.getById(shipmentDto.getDestinationWarehouse()));
        shipment.setDateOfDeparture(LocalDate.parse(shipmentDto.getDepartureDate(), formatter));
        shipment.setDateOfArrival(LocalDate.parse(shipmentDto.getArrivalDate(), formatter));
    }
}
