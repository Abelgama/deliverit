package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.ParcelDeliveryOption;
import com.telerikacademy.web.deliverit.models.ShipmentStatus;
import com.telerikacademy.web.deliverit.repositoies.contracts.ParcelDeliveryOptionRepository;
import com.telerikacademy.web.deliverit.repositoies.contracts.ShipmentStatusRespository;
import com.telerikacademy.web.deliverit.services.contracts.ParcelDeliveryOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
@Component
public class ParcelDeliveryOptionServiceImpl implements ParcelDeliveryOptionService {

    private final ParcelDeliveryOptionRepository deliveryRepository;

    @Autowired
    public ParcelDeliveryOptionServiceImpl(ParcelDeliveryOptionRepository deliveryRepository) {
        this.deliveryRepository = deliveryRepository;
    }

    @Override
    public List<ParcelDeliveryOption> getAll() {
        return deliveryRepository.getAll();
    }

    @Override
    public ParcelDeliveryOption getById(int id) {
        try {
            return deliveryRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
