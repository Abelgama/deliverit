package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Warehouse;

import java.util.List;

public interface WarehouseService {
    List<Warehouse> getAll();

    Warehouse getById(int id);

    void delete(int id);

    void update(Warehouse warehouse, Person person);

    void create(Warehouse warehouse, Person person);
}
