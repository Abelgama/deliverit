package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositoies.contracts.CityRepository;
import com.telerikacademy.web.deliverit.services.contracts.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository repository) {
        this.cityRepository = repository;
    }

    @Override
    public List<City> getAll(){
        return cityRepository.getAll();
    }

    @Override
    public City getById(int id){
        try {
            return cityRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }

    public City getByName (String name){
        try {
            return cityRepository.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,e.getMessage());
        }
    }
}
