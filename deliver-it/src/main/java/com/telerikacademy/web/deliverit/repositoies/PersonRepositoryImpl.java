package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.repositoies.contracts.PersonRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class PersonRepositoryImpl implements PersonRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PersonRepositoryImpl(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;

    }

    @Override
    public List<Person> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Person> query = session.createQuery(" from Person", Person.class);
            return query.list();
        }
    }

    @Override
    public Person getById(int id) {
        try (Session session = sessionFactory.openSession()){
            Person person = session.get(Person.class,id);
            if (person ==null){
                throw new EntityNotFoundException("Person",id);
            }
            return person;
        }
    }
    @Override
    public List<Person> getByEmail(String email) {
        try (Session session = sessionFactory.openSession()){
            Query<Person> query = session.createQuery("from Person where email = :email",Person.class);
            query.setParameter("email", email);
            return query.list();
        }
    }

    @Override
    public List<Person> searchByCustomerName(Optional<String> firstName,
                                              Optional<String> lastName) {

        String baseQuery = "from Person p ";
        if (firstName.isPresent()&& lastName.isPresent()){
            baseQuery +="where p.firstName =:firstName or p.lastName =:firstName or p.email like concat('%',:firstName,'%') or p.firstName =:lastName and p.lastName =:lastName and p.email like concat('%',:lastName,'%') ";
        }else if (firstName.isPresent()){
            baseQuery +="where p.firstName =:firstName or p.lastName =:firstName or p.email like concat('%',:firstName,'%')";
        }else if (lastName.isPresent()){
            baseQuery +="where p.firstName =:lastName or p.lastName =:lastName or p.email like concat('%',:lastName,'%')";
        }

        try (Session session = sessionFactory.openSession()){

            Query<Person> query = session.createQuery(baseQuery,Person.class);

            if (firstName.isPresent()&& lastName.isPresent()){
                query.setParameter("firstName", firstName.orElse(""));
                query.setParameter("lastName", lastName.orElse(""));
            }else if (firstName.isPresent()){
                query.setParameter("firstName", firstName.orElse(""));
            }else if (lastName.isPresent()){
                query.setParameter("lastName", lastName.orElse(""));
            }

            return query.list();
        }
    }

    @Override
    public void delete(int id) {
        Person personToDelete = getById(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.delete(personToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(Person person) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(person);
            session.getTransaction().commit();
        }
    }

    @Override
    public void create(Person person) {
        try (Session session = sessionFactory.openSession()) {
            session.save(person);
        }
    }
}
