package com.telerikacademy.web.deliverit.repositoies;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.City;
import com.telerikacademy.web.deliverit.repositoies.contracts.CityRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CityRepositoryImpl implements CityRepository {

    private final SessionFactory sessionFactory;

    public CityRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<City> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<City> queue = session.createQuery("from City", City.class);
            return queue.list();
        }
    }

    @Override
    public City getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            City city = session.get(City.class, id);
            if (city == null) {
                throw new EntityNotFoundException("City", id);
            }
            return city;
        }
    }

    public City getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            City city = session.get(City.class, name);
            if (city == null) {
                throw new EntityNotFoundException("City", name, "");
            }
            return city;
        }
    }
}
