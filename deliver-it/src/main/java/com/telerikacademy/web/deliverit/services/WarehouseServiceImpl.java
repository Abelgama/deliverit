package com.telerikacademy.web.deliverit.services;

import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.Warehouse;
import com.telerikacademy.web.deliverit.repositoies.contracts.WarehouseRepository;
import com.telerikacademy.web.deliverit.services.contracts.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Component
public class WarehouseServiceImpl implements WarehouseService {
    private final WarehouseRepository warehouseRepository;

    @Autowired
    public WarehouseServiceImpl(WarehouseRepository repository) {
        this.warehouseRepository = repository;
    }

    @Override
    public List<Warehouse> getAll() {
        return warehouseRepository.getAll();
    }

    @Override
    public Warehouse getById(int id) {
        try {
            return warehouseRepository.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @Override
    public void delete(int id) {
        warehouseRepository.delete(id);
    }

    @Override
    public void update(Warehouse warehouse, Person person) {
        if (!person.isEmployee()) {
            throw new UnsupportedOperationException("Only employee can update warehouse");
        }
        warehouseRepository.update(warehouse);
    }

    @Override
    public void create(Warehouse warehouse, Person person) {
        if (!person.isEmployee()) {
            throw new UnsupportedOperationException("Only employee can create warehouse");
        }

        boolean duplicateExist = true;
        try {
            Warehouse existingWarehouse = warehouseRepository.getById(warehouse.getId());
            if (existingWarehouse.getId() == warehouse.getId()) {
                duplicateExist = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExist = false;
        }
        if (duplicateExist) {
            throw new DuplicateEntityExeption("Warehouse", "Address", warehouse.getName());
        }
        warehouseRepository.create(warehouse);
    }
}
