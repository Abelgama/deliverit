package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Shipment;

import java.util.List;
import java.util.Optional;

public interface ParcelRepository {

    List<Parcel> getAll();

    Parcel getById(int id);

    List<Parcel> getByWeight(int weight);

    List<Parcel> getByCustomer(int customerId);

    List<Parcel> getByWarehouse(int warehouseId);

    List<Parcel> getByCategory(int category);

    List<Parcel> getByMultipleCriteria(Optional<Integer> customerId,
                                       Optional<Integer> category);

    List<Parcel> getMyParcels(int statusId , String email);

    void delete(int id);

    void update(Parcel parcel);

    void create(Parcel parcel);
}
