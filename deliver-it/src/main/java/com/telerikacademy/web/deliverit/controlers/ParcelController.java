package com.telerikacademy.web.deliverit.controlers;

import com.telerikacademy.web.deliverit.controlers.helpers.AuthenticationHelper;
import com.telerikacademy.web.deliverit.exeptions.DuplicateEntityExeption;
import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import com.telerikacademy.web.deliverit.models.dtos.CreateParcelsDto;
import com.telerikacademy.web.deliverit.services.contracts.ParcelService;
import com.telerikacademy.web.deliverit.utils.ParcelMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api/parcels")
public class ParcelController {

    private final ParcelService parcelService;
    private final ParcelMapper mapper;
    private final AuthenticationHelper authenticationHelper;

    public ParcelController(ParcelService parcelService,ParcelMapper mapper,AuthenticationHelper authenticationHelper ) {
        this.parcelService = parcelService;
        this.mapper = mapper;
        this.authenticationHelper=authenticationHelper;
    }

    @GetMapping
    public List<Parcel> getAll() {
        return parcelService.getAll();
    }

    @GetMapping("/{id}")
    public Parcel getById(@PathVariable int id) {
        try {
            return parcelService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            parcelService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/personal")
    public List<Parcel> getMyParcels(@RequestHeader HttpHeaders header,@RequestParam int statusId,
                                     @RequestParam String email) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            return parcelService.getMyParcels(statusId,email,person);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/weight")
    public List<Parcel> getByWeight(@RequestParam int weight) {
        try {
            return parcelService.getByWeight(weight);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

   @GetMapping("/customer")
    public List<Parcel> getByCustomer(@RequestParam int customerId) {
        try {
            return parcelService.getByCustomer(customerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/warehouse")
    public List<Parcel> getByWarehouse(@RequestParam int warehouseId) {
        try {
            return parcelService.getByWarehouse(warehouseId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/category")
    public List<Parcel> getByCategory(@RequestParam int category) {
        try {
            return parcelService.getByCategory(category);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

   @GetMapping("/multiple")
    public List<Parcel> getByMultipleCriteria(@RequestParam (required = false) Optional<Integer> customerId,
                                              @RequestParam (required = false) Optional<Integer> category) {
        try {
            return parcelService.getByMultipleCriteria(customerId,category);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Parcel update(@RequestHeader HttpHeaders header,@PathVariable int id, @Valid @RequestBody CreateParcelsDto parcelDto) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            Parcel parcel = mapper.parcelFromDto(parcelDto,id);
            parcelService.update(parcel,person);
            return parcel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityExeption e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping()
    public Parcel create(@RequestHeader HttpHeaders header,@Valid @RequestBody CreateParcelsDto parcelDto) {
        try {
            Person person = authenticationHelper.tryGetPerson(header);
            Parcel parcel = mapper.parcelFromDto(parcelDto);
            parcelService.create(parcel,person);
            return parcel;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityExeption e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }catch (UnsupportedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
