package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;
import java.util.Optional;

public interface PersonRepository {

    List<Person> getAll();

    List<Person> getByEmail(String email);

    List<Person> searchByCustomerName(Optional<String> firstName,
                                      Optional<String> lastName);

    Person getById(int id);

    void delete(int id);

    void update(Person person);

    void create(Person person);
}
