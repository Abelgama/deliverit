package com.telerikacademy.web.deliverit.utils;

import com.telerikacademy.web.deliverit.models.*;
import com.telerikacademy.web.deliverit.models.dtos.CreateParcelsDto;
import com.telerikacademy.web.deliverit.models.dtos.CreatePersonDto;
import com.telerikacademy.web.deliverit.repositoies.contracts.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonMapper {
    private final PersonRepository personRepository;
    private final PersonRoleRepository roleRepository;
    private final CityRepository cityRepository;


    @Autowired
    public PersonMapper(PersonRepository personRepository, PersonRoleRepository roleRepository,CityRepository cityRepository){
        this.personRepository = personRepository;
        this.roleRepository = roleRepository;
        this.cityRepository = cityRepository;

    }

    public Person personFromDto(CreatePersonDto personDto) {
        Person person  = new Person();
        dtoToObject(personDto, person);
        return person;
    }

    public Person personFromDto(CreatePersonDto personDto, int id) {
        Person person  = personRepository.getById(id);
        dtoToObject(personDto, person);
        return person;
    }

    private void dtoToObject(CreatePersonDto personDto, Person person ) {
        City city = cityRepository.getById(personDto.getCityId());
        person.setCity(city);

        PersonRole role = roleRepository.getById(personDto.getCityId());
        person.setRole(role);

        person.setEmail(personDto.getEmail());
        person.setLastName(personDto.getLastName());
        person.setFirstName(personDto.getFirstName());
        person.setStreetName(personDto.getStreetName());


    }

}
