package com.telerikacademy.web.deliverit.repositoies.contracts;

import com.telerikacademy.web.deliverit.models.ParcelDeliveryOption;

import java.util.List;

public interface ParcelDeliveryOptionRepository {

    List<ParcelDeliveryOption> getAll();

    ParcelDeliveryOption getById(int id);
}
