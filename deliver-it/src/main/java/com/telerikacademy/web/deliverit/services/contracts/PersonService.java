package com.telerikacademy.web.deliverit.services.contracts;

import com.telerikacademy.web.deliverit.exeptions.EntityNotFoundException;
import com.telerikacademy.web.deliverit.models.Parcel;
import com.telerikacademy.web.deliverit.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

public interface PersonService {

    List<Person> getAll();

    Person getById(int id);

    List<Person> getByEmail(String email);

    List<Person> searchByCustomerName(Optional<String> firstName, Optional<String> lastName);

    void delete(int id);

    void update(Person person);

    void create(Person person);


}
