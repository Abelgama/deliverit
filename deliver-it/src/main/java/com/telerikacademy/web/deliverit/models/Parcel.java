package com.telerikacademy.web.deliverit.models;


import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.Calendar;
@Entity
@Table(name = "parcels" )
public class Parcel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="parcel_id")
    @Positive(message = "Id should be positive")
    private int id;


    @ManyToOne
    @JoinColumn(name = "customer_id")
    @NotBlank(message = "Customer can not be empty")
    private Person customer;

    @OneToOne
    @JoinColumn(name="to_warehouse_id")
    @NotBlank(message = "Warehouse can not be empty")
    private Warehouse warehouse;

    @OneToOne
    @JoinColumn(name="category_id")
    @NotBlank(message = "Category can not be empty")
    private ParcelCategory category;

    @Column(name="weight")
    @Positive(message = "Weight should be positive")
    @NotBlank(message = "Weight can not be empty")
    private double weight;

    @OneToOne
    @JoinColumn(name="shipments_id")
    private Shipment shipment;

    @OneToOne
    @JoinColumn(name="delivery_option")
    @NotBlank(message = "Delivery option can not be empty")
    private ParcelDeliveryOption option;

    public Parcel(){

    }
    public Parcel(int id, Person customer, Warehouse warehouse, ParcelCategory category, double weight, Shipment shipment,ParcelDeliveryOption option) {
        this.id = id;
        this.customer = customer;
        this.warehouse = warehouse;
        this.category = category;
        this.weight = weight;
        this.shipment = shipment;
        this.option = option;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Person getCustomer() {
        return customer;
    }

    public void setCustomer(Person customer) {
        this.customer = customer;
    }

    public Warehouse getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(Warehouse warehouse) {
        this.warehouse = warehouse;
    }

    public ParcelCategory getCategory() {
        return category;
    }

    public void setCategory(ParcelCategory category) {
        this.category = category;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Shipment getShipment() {
        return shipment;
    }

    public void setShipment(Shipment shipment) {
        this.shipment = shipment;
    }
    public ParcelDeliveryOption getDeliveryOption() {
        return option;
    }

    public void setDeliveryOption(ParcelDeliveryOption option) {
        this.option = option;
    }



}
